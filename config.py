detect_cls              = ['car','person','facial']         # detect type
cls_has_night           = ['car']                           # detect type which has night attribute
do_crop_aspect_ratio    = ['person']
do_get_roi              = ['facial']
do_lamp_filter          = ['car']
do_white_balance        = {'day':['person'], 'night':['person','car']}
