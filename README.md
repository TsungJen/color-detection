# iRONYUN Color Detection

A Color Detection Engine for Object Detector

## Getting Started

Please have opencv2, PIL, numpy, matplotlib intalled in your system

### Prerequisites

Please have opencv2, PIL, numpy, matplotlib intalled in your system


## Running the tests

'''
python search_color.py
'''

### Running the evaluation

Explain what these tests test and why

```
python tools/color_Eval.py
```

### Running optimization over a new dataset

'''
mv 'your dataset' tools/opt_color/.
config target folder in tools/opt_color/config.py
cd tools/opt_color
python opt_color.py
```

## Deployment

Add additional notes about how to deploy this on a live system


## Authors

* **TsungJen Hsu**
